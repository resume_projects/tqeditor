
# tqEditer
This is a simple text editor made from scratch in C++ using Qt framework .
### Features

- Support Standard file manipulation features such as creating new file, opening files and saveing files.
- All the basic features of a text editor such as cut, copy, paste. 
- Text editing features such as bold, italic, underline, changing text color/ type. 
- Supports find and replace within the text with regular expressions support

### Screenshots 
![basic functionality](shots/test2.gif)
![text functionality](shots/test3.gif)
![text modification](shots/textchange.png)
![find and replace](shots/findreplace.png)


