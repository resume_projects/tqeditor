#include "finddialogbox.h"
#include "ui_finddialogbox.h"

FindDialogBox::FindDialogBox(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FindDialogBox)
{
    ui->setupUi(this);
    connect(ui->buttonBox, &QDialogButtonBox::accepted ,this, &FindDialogBox::btn_box_accept);
    connect(ui->buttonBox, &QDialogButtonBox::rejected ,this, &FindDialogBox::btn_box_reject);
    init();
    save();
}

FindDialogBox::~FindDialogBox()
{
    delete ui;
}

void FindDialogBox::btn_box_accept()
{
    save();
    accept();
}

void FindDialogBox::btn_box_reject()
{
    reject();
}

void FindDialogBox::go_back()
{
    save();
    m_backwards = true;
    accept();
}

bool FindDialogBox::backwards() const
{
    return m_backwards;
}

void FindDialogBox::setBackwards(bool newBackwards)
{
    m_backwards = newBackwards;
}

void FindDialogBox::init()
{
    QPushButton* backbtn = new QPushButton("Back", this);
    connect(backbtn, &QPushButton::clicked, this, &FindDialogBox::go_back);
    ui->buttonBox->addButton(backbtn, QDialogButtonBox::ActionRole);
}

void FindDialogBox::save()
{
    FindDialogBox::setTxt(ui->lineEdit->text());
    FindDialogBox::setMatch_case(ui->chkMatchCase->isChecked());
    FindDialogBox::setMatch_wholewords(ui->chkMatchWhole->isChecked());
    FindDialogBox::setBackwards(false);
}

const QString &FindDialogBox::txt() const
{
    return m_txt;
}

void FindDialogBox::setTxt(const QString &newTxt)
{
    m_txt = newTxt;
}

bool FindDialogBox::match_case() const
{
    return m_match_case;
}

void FindDialogBox::setMatch_case(bool newMatch_case)
{
    m_match_case = newMatch_case;
}

bool FindDialogBox::match_wholewords() const
{
    return m_match_wholewords;
}

void FindDialogBox::setMatch_wholewords(bool newMatch_wholewords)
{
    m_match_wholewords = newMatch_wholewords;
}
