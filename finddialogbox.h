#ifndef FINDDIALOGBOX_H
#define FINDDIALOGBOX_H

#include <QDialog>
#include <QPushButton>

namespace Ui {
class FindDialogBox;
}

class FindDialogBox : public QDialog
{
    Q_OBJECT

public:
    explicit FindDialogBox(QWidget *parent = nullptr);
    ~FindDialogBox();

    bool match_wholewords() const;
    void setMatch_wholewords(bool newMatch_wholewords);

    bool match_case() const;
    void setMatch_case(bool newMatch_case);

    const QString &txt() const;
    void setTxt(const QString &newTxt);

    bool backwards() const;
    void setBackwards(bool newBackwards);

private slots:
    void btn_box_accept();
    void btn_box_reject();
    void go_back();


private:
    Ui::FindDialogBox *ui;
    QString m_txt;

    bool m_match_case, m_match_wholewords, m_backwards;
    void init();
    void save();


};

#endif // FINDDIALOGBOX_H
