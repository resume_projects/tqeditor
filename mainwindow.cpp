#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(ui->textEdit);

    //signals and slots

    connect(ui->actionNew,         &QAction::triggered, this, &MainWindow::new_file         );
    connect(ui->actionOpen,        &QAction::triggered, this, &MainWindow::open_file        );
    connect(ui->actionSave,        &QAction::triggered, this, &MainWindow::savefile_action  );
    connect(ui->actionSave_As,     &QAction::triggered, this, &MainWindow::save_as          );
    connect(ui->actionExit,        &QAction::triggered, this, &MainWindow::close_action     );
    connect(ui->actionCut,         &QAction::triggered, this, &MainWindow::sel_cut          );
    connect(ui->actionCopy,        &QAction::triggered, this, &MainWindow::sel_copy         );
    connect(ui->actionPaste,       &QAction::triggered, this, &MainWindow::sel_paste        );
    connect(ui->actionUndo,        &QAction::triggered, this, &MainWindow::undo_changes     );
    connect(ui->actionRedo,        &QAction::triggered, this, &MainWindow::redo_changes     );
    connect(ui->actionSelect_all,  &QAction::triggered, this, &MainWindow::sel_all          );
    connect(ui->actionZoom_in,     &QAction::triggered, this, &MainWindow::zoom_in          );
    connect(ui->actionZoom_out,    &QAction::triggered, this, &MainWindow::zoom_out         );
    connect(ui->actionBold,        &QAction::triggered, this, &MainWindow::make_bold        );
    connect(ui->actionItalic,      &QAction::triggered, this, &MainWindow::make_italic      );
    connect(ui->actionUnderline,   &QAction::triggered, this, &MainWindow::make_underlined  );
    connect(ui->actionStrike,      &QAction::triggered, this, &MainWindow::make_striked     );
    connect(ui->actionfind,        &QAction::triggered, this, &MainWindow::find_dialog      );
    connect(ui->actionReplace,     &QAction::triggered, this, &MainWindow::replace_dialog   );
    connect(ui->actionColor,       &QAction::triggered, this, &MainWindow::change_color     );
    connect(ui->actionFont,        &QAction::triggered, this, &MainWindow::change_font      );
    connect(ui->actionSource_Code, &QAction::triggered, this, &MainWindow::open_site        );

    connect(ui->textEdit,          &QTextEdit::textChanged, this, &MainWindow::text_change);
    new_file();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::text_change()
{
    text_changed=true;
    return ;
}

void MainWindow::open_file()
{
    checksave();
    QString chosen_path = QFileDialog::getOpenFileName(this, "Open File");
    if(chosen_path.isEmpty()) return ;
    QFile txtfile(chosen_path);
    if(!txtfile.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(this, "Failed opening file", txtfile.errorString() );
        ui->statusbar->showMessage("Failed opening the file!");
        return;
    }

    QTextStream buffer(&txtfile);

    ui->textEdit->setHtml(buffer.readAll());

    ui->statusbar->showMessage(m_path);
    text_changed = false;
    txtfile.close();
}

void MainWindow::save_file(QString chosen_path)
{
    QFile txtfile(chosen_path);
    if(!txtfile.open(QIODevice::WriteOnly)){
        QMessageBox::critical(this, "Failed to access the file", txtfile.errorString());
        ui->statusbar->showMessage("Failed saving the file! Please try save file as if nothing works.");
        return ;
    }
    QTextStream buffer(&txtfile);

    buffer << ui->textEdit->toHtml();
    m_path = chosen_path;
    ui->statusbar->showMessage(m_path);
    text_changed = false;
    txtfile.close();
}

void MainWindow::savefile_action()
{
    if(m_path.isEmpty()){
        save_as();
    }
    else{
        save_file(m_path);
    }
}

void MainWindow::save_as()
{
    QString chosen_path = QFileDialog::getSaveFileName(this, "Save location");
    if(chosen_path.isEmpty())
    {
        return ;
    }
    save_file(chosen_path);
}

void MainWindow::checksave()
{
    if(m_path.isEmpty() ){
        QMessageBox::StandardButton value = QMessageBox::question(this, "Unsaved file", "You have unsaved changes, would you like to save them ?");
        if(value==QMessageBox::StandardButton::No){
            return ;
        }
        save_as();
    }
    else if(text_changed){
        QMessageBox::StandardButton value = QMessageBox::question(this, "Unsaved file", "You have unsaved changes, would you like to save them ?");
        if(value==QMessageBox::StandardButton::No){
            return ;
        }
        save_file(m_path);
    }
}

void MainWindow::new_file()
{
    ui->textEdit->clear();
    ui->statusbar->showMessage("New file created!");
    m_path="";
    text_changed=false;

}

void MainWindow::sel_cut()
{
    ui->textEdit->cut();
}

void MainWindow::sel_copy()
{
    ui->textEdit->copy();

}

void MainWindow::sel_paste()
{
    ui->textEdit->paste();
}

void MainWindow::undo_changes()
{
    ui->textEdit->undo();
    text_changed = true;
}

void MainWindow::redo_changes()
{
    ui->textEdit->redo();
    text_changed = true;

}

void MainWindow::sel_all()
{
    ui->textEdit->selectAll();
}

void MainWindow::zoom_in()
{
    ui->textEdit->zoomIn();
}

void MainWindow::zoom_out()
{
    ui->textEdit->zoomOut();
}

void MainWindow::make_bold()
{
    QFont cur_font = ui->textEdit->currentFont();
    if(cur_font.bold())
    {
        cur_font.setBold(false);
    }
    else
    {
        cur_font.setBold(true);
    }
    ui->textEdit->setCurrentFont(cur_font);
    text_changed = true;
}

void MainWindow::make_italic()
{
    QFont cur_font = ui->textEdit->currentFont();
    if(cur_font.italic())
    {
        cur_font.setItalic(false);
    }
    else
    {
        cur_font.setItalic(true);
    }
    ui->textEdit->setCurrentFont(cur_font);
    text_changed = true;
}

void MainWindow::make_underlined()
{
    QFont cur_font = ui->textEdit->currentFont();
    if(cur_font.underline())
    {
        cur_font.setUnderline(false);
    }
    else
    {
        cur_font.setUnderline(true);
    }
    ui->textEdit->setCurrentFont(cur_font);
    text_changed = true;
}

void MainWindow::make_striked()
{
    QFont cur_font = ui->textEdit->currentFont();
    if(cur_font.strikeOut())
    {
        cur_font.setStrikeOut(false);
    }
    else
    {
        cur_font.setStrikeOut(true);
    }
    ui->textEdit->setCurrentFont(cur_font);
    text_changed = true;
}

void MainWindow::find_dialog()
{
    FindDialogBox* find_dlg = new FindDialogBox(this);
    if(!find_dlg->exec()) return ; // user rejected , do nothing
    auto n_cursor = ui->textEdit->textCursor();
    auto old_position = ui->textEdit->textCursor().position();
    n_cursor.movePosition(QTextCursor::Start);
    ui->textEdit->setTextCursor(n_cursor);
    QTextDocument::FindFlags flags;
    if(find_dlg->match_case())
    {
        flags |= QTextDocument::FindFlag::FindCaseSensitively;
    }
    if(find_dlg->match_wholewords())
    {
        flags |= QTextDocument::FindFlag::FindWholeWords;
    }
    if(find_dlg->backwards())
    {
        flags |= QTextDocument::FindFlag::FindBackward;
    }

    bool found = ui->textEdit->find(find_dlg->txt(), flags);
    if(!found)
    {
        QMessageBox::information(this, "Not Found", "No such text found in the document!");
        n_cursor.setPosition(old_position);
        ui->textEdit->setTextCursor(n_cursor);
    }
}

void MainWindow::replace_dialog()
{
    Replace_dialog_box* Replace_dlg = new Replace_dialog_box(this);
    if(!Replace_dlg->exec()) return ;
    auto n_cursor = ui->textEdit->textCursor();
    auto old_position = ui->textEdit->textCursor().position();

    n_cursor.movePosition(QTextCursor::Start);
    ui->textEdit->setTextCursor(n_cursor);
    if(Replace_dlg->replaceall())
    {
        QString txt = ui->textEdit->toHtml();
        txt = txt.replace(Replace_dlg->txt_find(), Replace_dlg->txt_replace());
        ui->textEdit->setHtml(txt);
        n_cursor.setPosition(old_position);
        ui->textEdit->setTextCursor(n_cursor);
        text_changed = true;
    }
    else
    {
        bool value = ui->textEdit->find(Replace_dlg->txt_find());
        if(!value)
        {
            QMessageBox::information(this, "Not Found", "No such text was found , exiting.");
            n_cursor.setPosition(old_position);
            ui->textEdit->setTextCursor(n_cursor);
            return ;
        }
        text_changed = true;
        QTextCursor sel_cursor = ui->textEdit->textCursor();
        sel_cursor.insertHtml(Replace_dlg->txt_replace());
        n_cursor.setPosition(old_position);
        ui->textEdit->setTextCursor(n_cursor);
        return ;
    }
}

void MainWindow::change_color()
{
    QColor sel_col = QColorDialog::getColor(ui->textEdit->currentCharFormat().foreground().color(), this, "Select color" );
    ui->textEdit->setTextColor(sel_col);
}

void MainWindow::change_font()
{
    bool font_changed;
    QFont new_font = QFontDialog::getFont(&font_changed, ui->textEdit->currentFont(), this, "Select a font");
    if(font_changed){
        ui->textEdit->setCurrentFont(new_font);
    }
    return ;
}

void MainWindow::open_site()
{
    QDesktopServices::openUrl(QUrl("https://www.codeforces.com"));
    return;
}

void MainWindow::close_action()
{
    close();
    return;
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    checksave();
    QMessageBox::StandardButton resBtn = QMessageBox::question( this,"Exit the application" ,
                                                               tr("Are you sure that you want to exit?\n"),
                                                               QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                               QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
    }
}
