#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QFileDialog>
#include <QTextEdit>
#include <QColorDialog>
#include <QFontDialog>
#include <QDesktopServices>
#include <QUrl>
#include <QApplication>



#include "finddialogbox.h"
#include "replace_dialog_box.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    bool text_changed;
    QString m_path;


private slots:
    void text_change();
    void open_file();
    void save_file(QString chosen_path);
    void savefile_action();
    void save_as();
    void checksave();
    void new_file();
    void sel_cut();
    void sel_copy();
    void sel_paste();
    void close_action();
    void undo_changes();
    void redo_changes();
    void sel_all();
    void zoom_in();
    void zoom_out();
    void make_bold();
    void make_italic();
    void make_underlined();
    void make_striked();
    void find_dialog();
    void replace_dialog();
    void change_color();
    void change_font();
    void open_site();

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);
};
#endif // MAINWINDOW_H
