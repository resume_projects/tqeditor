#include "replace_dialog_box.h"
#include "ui_replace_dialog_box.h"

Replace_dialog_box::Replace_dialog_box(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Replace_dialog_box)
{
    ui->setupUi(this);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &Replace_dialog_box::canceled);


    init();
    save();
}

Replace_dialog_box::~Replace_dialog_box()
{
    delete ui;
}

void Replace_dialog_box::canceled()
{
    reject();
}

void Replace_dialog_box::replace()
{
    save();
    accept();
}

void Replace_dialog_box::replace_all()
{
    save();
    m_replaceall = true;
    accept();
}

bool Replace_dialog_box::replaceall() const
{
    return m_replaceall;
}

void Replace_dialog_box::setReplaceall(bool newReplaceall)
{
    m_replaceall = newReplaceall;
}

void Replace_dialog_box::init()
{
    QPushButton* btn_Replace = new QPushButton("Replace once", this);
    QPushButton* btn_ReplaceAll = new QPushButton("Replace all", this);
    ui->buttonBox->addButton(btn_Replace, QDialogButtonBox::ButtonRole::ActionRole);
    ui->buttonBox->addButton(btn_ReplaceAll, QDialogButtonBox::ButtonRole::ActionRole);


    connect(btn_Replace   , &QPushButton::clicked,  this, &Replace_dialog_box::replace     );
    connect(btn_ReplaceAll, &QPushButton::clicked,  this, &Replace_dialog_box::replace_all );
}

void Replace_dialog_box::save()
{
    setTxt_find(ui->Find_txt->text());
    setTxt_replace(ui->Replace_txt->text());
    setReplaceall(false);
}


const QString &Replace_dialog_box::txt_replace() const
{
    return m_txt_replace;
}

void Replace_dialog_box::setTxt_replace(const QString &newTxt_replace)
{
    m_txt_replace = newTxt_replace;
}

const QString &Replace_dialog_box::txt_find() const
{
    return m_txt_find;
}

void Replace_dialog_box::setTxt_find(const QString &newTxt_find)
{
    m_txt_find = newTxt_find;
}
