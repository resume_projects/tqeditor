#ifndef REPLACE_DIALOG_BOX_H
#define REPLACE_DIALOG_BOX_H

#include <QDialog>
#include <QPushButton>

namespace Ui {
class Replace_dialog_box;
}

class Replace_dialog_box : public QDialog
{
    Q_OBJECT

public:
    explicit Replace_dialog_box(QWidget *parent = nullptr);
    ~Replace_dialog_box();




    const QString &txt_find() const;
    void setTxt_find(const QString &newTxt_find);

    const QString &txt_replace() const;
    void setTxt_replace(const QString &newTxt_replace);

    bool replaceall() const;
    void setReplaceall(bool newReplaceall);

private slots:
    void canceled();
    void replace();
    void replace_all();


private:
    Ui::Replace_dialog_box *ui;

    bool m_replaceall;
    QString m_txt_find;
    QString m_txt_replace;


    void init();
    void save();
};

#endif // REPLACE_DIALOG_BOX_H
